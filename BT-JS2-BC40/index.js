// Bài 1
function tienLuong(){
const luongNgay = 100000;
var ngayLam = document.getElementById("txt-ngay-lam").value * 1;
var tongTien = luongNgay * ngayLam;
document.getElementById("result1").innerHTML = `<h2 class="container text-success ">Tiền Lương nhân viên là : ${tongTien} VNĐ</h2>`;
}


//Bài 2
 function giaTri(){
var soNhat = document.getElementById("txt-so-1").value * 1;
var soHai = document.getElementById("txt-so-2").value * 1;
var soBa = document.getElementById("txt-so-3").value * 1;
var soBon = document.getElementById("txt-so-4").value * 1;
var soNam = document.getElementById("txt-so-5").value * 1;
var ketQua = (soNhat + soHai + soBa +soBon +soNam) / 5;
document.getElementById("result2").innerHTML = `<h2 class="container text-danger">Giá trị trung bình là : ${ketQua}</h2>`
 }

//Bài 3
function doiTien(){
const tienViet = 23500;
var tienDo = document.getElementById("txt-tien-doi").value * 1;
var ketQuaDoi = tienDo * tienViet;
document.getElementById("result3").innerHTML = `<h2 class="container text-warning">Quy đổi sang tiền Việt là : ${ketQuaDoi} VNĐ</h2>`
}


// Bài 4
function tinhHcn(){
var chieuDai = document.getElementById("txt-chieu-dai").value * 1;
var chieuRong = document.getElementById("txt-chieu-rong").value * 1;
var dienTich = chieuDai * chieuRong;
var chuVi = (chieuDai + chieuRong) * 2;
document.getElementById("result4").innerHTML = `<h2 class="container text-primary ">Diện tích HCN là:${dienTich} <br />Chu vi HCN là: ${chuVi}</h2>`
}


// Bài 5
function tinhKySo(){
var so = document.getElementById("txt-ky-so").value * 1;
var hangChuc = Math.floor(so/10);
var hangDonVi = so % 10 ;
var kySo = hangChuc + hangDonVi;
document.getElementById("result5").innerHTML =`<h2 class="text-danger container">Tổng hai ký số là: ${kySo}</h2>`
}